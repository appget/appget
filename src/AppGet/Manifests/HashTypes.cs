﻿namespace AppGet.Manifests
{
    public enum HashTypes
    {
        Md5,
        Sha1,
        Sha256,
    }
}