﻿namespace AppGet.Manifests
{
    public enum InstallMethodTypes
    {
        Unknown,
        Custom,
        Zip,
        MSI,
        Wix,
        Inno,
        InstallShield,
        NSIS,
        InstallBuilder,
        Squirrel
    }
}